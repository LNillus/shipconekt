﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class StatTip : MonoBehaviour
{
    public void OpenTip(GameObject overlapTip)
    {
        overlapTip.SetActive(true);
    }

    public void CloseTip(GameObject overlapTip)
    {
        overlapTip.SetActive(false);
    }
}
