﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InfoPanelReferences : MonoBehaviour
{
    public TextMeshProUGUI price;
    public TextMeshProUGUI damages;
    public TextMeshProUGUI range;
    public TextMeshProUGUI fireRate;
    public TextMeshProUGUI bulletSpeed;
    public TextMeshProUGUI reloadTime;
    public TextMeshProUGUI hp;
    public TextMeshProUGUI armor;
    public TextMeshProUGUI weight;

    public static InfoPanelReferences instance;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        }

    public void WriteInfos(ModuleScriptable data)
    {
        if(data != null)
        {
            //moduleName.text = data.moduleName;
            price.text = data.price.ToString("F0") + " $";
            damages.text = data.damages.ToString("F2");
            range.text = data.range.ToString("F2");
            fireRate.text = data.fireRate.ToString("F2") + "/s";
            reloadTime.text = data.reloadTime.ToString("F2") + " S";
            bulletSpeed.text = data.bulletSpeed.ToString("F2");
            hp.text = "+ " + data.hp.ToString("F0");
            armor.text = "- " + data.armor.ToString("F0");
            weight.text = data.weight.ToString("F0") + " KG";
        }
    }

}
