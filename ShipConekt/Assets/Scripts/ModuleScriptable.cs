﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ModuleScriptable : ScriptableObject
{
    public string moduleName;
    public int price;
    public float damages;
    public int range;
    public float fireRate;
    public float bulletSpeed;
    public float reloadTime;
    public float hp;
    public int armor;
    public int weight;
}
