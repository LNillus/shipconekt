﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ConstructionManager : MonoBehaviour
{
    public bool debug;

    bool isHandlingModule;
    GameObject currentSelection;
    Vector3 currentSelectionClickPos;

    //CellInfos[,] grid;

    public List<CellInfosList> grid = new List<CellInfosList>();

    public Vector2Int gridDimensions;
    [ReadOnly, ShowIf("debug")]
    public float maxCameraZoom;
    [Range(0.01f, 4f)]
    public float maxZoomRatio;

    public GameObject gridCellPrefab;
    public Vector3Int cameraGridOffset;

    Vector3 initialCameraPos;

    public AnimationCurve zoomCurve;
    public float zoomAimForce;

    // Start is called before the first frame update
    void Start()
    {
        DrawGrid(gridDimensions.x, gridDimensions.y, Vector3.zero);
    }

    // Update is called once per frame
    void Update()
    {
        HandleModule();
        FindCellOnClick();
        CameraZoom();
    }

    public void HandleModule()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit2D[] hits = Physics2D.GetRayIntersectionAll(ray, Mathf.Infinity);

        if(Input.GetMouseButtonDown(0) && hits.Length > 0  && isHandlingModule == false)
        {
            for (int i = 0; i < hits.Length; i++)
            {
                if(hits[i].collider.CompareTag("Module"))
                {
                    currentSelection = hits[i].collider.gameObject;
                    currentSelectionClickPos = currentSelection.transform.position;
                    isHandlingModule = true;
                    return;
                }
            }

            currentSelection = null;
            isHandlingModule = false;
        }

        if (Input.GetMouseButton(0) && isHandlingModule)
        {

            Vector3 mousePos = Input.mousePosition;
            mousePos.z = 10;

            Debug.Log(currentSelection.transform.GetComponent<PolygonCollider2D>().bounds.extents);

            currentSelection.transform.position = Camera.main.ScreenToWorldPoint(mousePos) + new Vector3(currentSelection.transform.GetComponent<PolygonCollider2D>().bounds.extents.x - 0.5f, -currentSelection.transform.GetComponent<PolygonCollider2D>().bounds.extents.y+0.5f, 0);
        }

        if (Input.GetMouseButtonUp(0) && isHandlingModule)
        {
            DropModule();
            isHandlingModule = false;
        }
    }

    public void DropModule()
    {
        currentSelection = null;
    }

    public void FindCellOnClick()
    {
        if(Input.GetMouseButton(0))
        {
            for (int i = 0; i < grid.Count; i++)
            {
                for (int j = 0; j < grid[i].cellInfosY.Count; j++)
                {
                    if (new Vector3((int)grid[i].cellInfosY[j].cellPos.x, (int)grid[i].cellInfosY[j].cellPos.y, 0) == new Vector3((int)(Camera.main.ScreenToWorldPoint(Input.mousePosition).x + 0.5f), (int)(Camera.main.ScreenToWorldPoint(Input.mousePosition).y + 0.5f), 0))
                    {
                        if (currentSelection != null) //Condition à fix
                        {
                            if (grid[i].cellInfosY[j].cellModule != null && grid[i].cellInfosY[j].cellModule != currentSelection.gameObject)
                            {
                                currentSelection.transform.position = currentSelectionClickPos;
                                return;
                            }
                            else
                            {
                                grid[i].cellInfosY[j].SetCellModule(currentSelection.gameObject);
                                grid[i].cellInfosY[j].isOccupied = true;
                                currentSelection.transform.position = grid[i].cellInfosY[j].cellPos;
                                currentSelectionClickPos = currentSelection.transform.position;
                            }
                        }
                    }
                }
            }
        }
    }

    public void DrawGrid(int xCells, int yCells, Vector3 startingPoint)
    {
        //grid = new CellInfos[xCells, yCells];       

        for (int i = 0; i < xCells; i++)
        {
            grid.Add(new CellInfosList());           
            for (int j = 0; j < yCells; j++)
            {
                grid[i].cellInfosY.Add(new CellInfos());
                grid[i].cellInfosY[j].SetCellPos(startingPoint + new Vector3(i, j, 0));
                GameObject go = Instantiate(gridCellPrefab, grid[i].cellInfosY[j].cellPos, Quaternion.identity);
            }
        }

        Vector3 gridMid = grid[xCells / 2].cellInfosY[yCells / 2].cellPos;

        Camera.main.orthographicSize = 7.5f/10f*(xCells>yCells ? xCells : yCells);
        maxCameraZoom = Camera.main.orthographicSize;

        Camera.main.transform.position = new Vector3(gridMid.x + cameraGridOffset.x, gridMid.y + cameraGridOffset.y, cameraGridOffset.z);
        initialCameraPos = Camera.main.transform.position;
    }

    float lastOrthographicSize;
    public void CameraZoom()
    {
        if (Input.mouseScrollDelta != Vector2.zero)
        {
            Camera.main.orthographicSize -= Input.mouseScrollDelta.y;
            Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, maxCameraZoom / maxZoomRatio, maxCameraZoom * maxZoomRatio);

            if (lastOrthographicSize != Camera.main.orthographicSize)
            {
                Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                float movingStep = ((maxCameraZoom * maxZoomRatio) - (maxCameraZoom / maxZoomRatio));
                float currentStep = (Camera.main.orthographicSize - (maxCameraZoom / maxZoomRatio));
                float zoomProgression = currentStep / movingStep; // = InverseLerp

                if (Input.mouseScrollDelta.y > 0)
                {
                    mousePos.x = Mathf.Clamp(mousePos.x, grid[0].cellInfosY[0].cellPos.x, grid[grid.Count-1].cellInfosY[grid[grid.Count - 1].cellInfosY.Count-1].cellPos.x);
                    mousePos.y = Mathf.Clamp(mousePos.y, grid[0].cellInfosY[0].cellPos.y, grid[grid.Count-1].cellInfosY[grid[grid.Count - 1].cellInfosY.Count-1].cellPos.y);

                    Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, mousePos, zoomCurve.Evaluate(zoomProgression) * zoomAimForce);                    
                }
                else
                {
                    Camera.main.transform.position = Vector3.MoveTowards(Camera.main.transform.position, initialCameraPos, zoomCurve.Evaluate(zoomProgression) * zoomAimForce);
                }
            }

            lastOrthographicSize = Camera.main.orthographicSize;
        }
    }
}

[System.Serializable]
public class CellInfos
{
    public Vector3 cellPos;
    public bool isOccupied;
    public GameObject cellModule;

    public void OccupationState(bool _value)
    {
        isOccupied = _value;
    }

    public void SetCellModule(GameObject _go)
    {
        cellModule = _go;
    }

    public void SetCellPos(Vector3 _vec)
    {
        cellPos = _vec;
    }

    public void RemoveCellModule()
    {
        cellModule = null;
    }
}

[System.Serializable]
public class CellInfosList
{
    public List<CellInfos> cellInfosY = new List<CellInfos>();
}
