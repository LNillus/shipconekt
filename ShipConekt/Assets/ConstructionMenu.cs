﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class ConstructionMenu : MonoBehaviour
{
    public Color selectionColor;

    public GameObject selectedModule;
    public GameObject infoPanel;

    public static ConstructionMenu instance;

    private void Awake()
    {
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DeselectModule()
    {
        if (selectedModule != null)
        {
            selectedModule.GetComponent<Image>().color = Color.white;
            selectedModule = null;
            infoPanel.SetActive(false);
        }
    }

    public void SelectModule()
    {       
        GameObject go = EventSystem.current.currentSelectedGameObject;

        Image image = go.GetComponent<Image>();

        if(image.color != selectionColor)
        {
            DeselectModule();
            infoPanel.SetActive(true);
            image.color = selectionColor;
        }
        else
        {
            infoPanel.SetActive(false);
            image.color = Color.white;
        }

        selectedModule = go;
        InfoPanel(go);
    }

    public void EnableDisable(GameObject _go)
    {
        _go.SetActive(!_go.activeSelf);

        if(_go.activeSelf == false)
        {
            DeselectModule();
        }
    }

    public void InfoPanel(GameObject _go)
    {
        if(selectedModule != null)
        {
            InfoPanelReferences.instance.WriteInfos(_go.GetComponent<Module>().moduleValues);
        }
    }
}
